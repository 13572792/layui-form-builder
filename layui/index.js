layui.config({
    base: 'layui/module/'
}).extend({
    sortable: 'formbuilder/sortable.min',
    formbuilder: 'formbuilder/formbuilder',
}).use(['formbuilder'], () => {
    let formbuilder = layui.formbuilder;

    /*根据ID渲染表单构造页面*/
    let inst = formbuilder.render('form-builder-container');

    /*获取表单构造的json数组结果（每个json对象表示一个表单元素）*/
    let jsonArr = inst.getJsonArr();

    /*根据json数组，生成表单页面*/
    inst.json2html('xxxxId', jsonArr);
});