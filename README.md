# layui-form-builder 表单构造工具

#### 一、介绍

拖拉拽生成layui表单元素，编辑元素json数据，实时生成元素预览。

页面左侧为可用组件库，中间为表单样式预览，右侧为组件属性。拖拉拽左侧组件到中间部位，点击选中组件后，右侧显示组件配置json。编辑json后，配置框失去焦点或点击"渲染表单"生成预览效果。点击预览效果区域组件左侧删除图标，可删除组件。

表单配置结果可以导出本地json数据，也可以获取josn数组后根据需求二次处理。

提示：可用组件不太多，大家可查阅**formbuilder.js**代码，自行添加更多组件。

![表单构造工具效果图](layui/images/form-tool.png "表单构造工具效果图")

#### 二、使用步骤

1. 拷贝"layui/module/formbuilder"文件夹到自己的项目
1. 引用表单构造工具
```
layui.config({
    base: 'layui/module/'
}).extend({
    sortable: 'formbuilder/sortable.min',
    formbuilder: 'formbuilder/formbuilder',
}).use(['formbuilder'], () => {
    let formbuilder = layui.formbuilder;

    /*根据ID渲染表单构造页面*/
    let inst = formbuilder.render('form-builder-container');

    /*获取表单构造的json数组结果（每个json对象表示一个表单元素）*/
    let jsonArr = inst.getJsonArr();

    /*根据json数组，生成表单页面*/
    inst.json2html('xxxxId', jsonArr);
});
```

#### 三、组件属性配置
**1.输入框**

```
{
  "active": true,
  "type": "input",
  "label": "输入框",
  "name": "input",
  "value": "",
  "placeholder": "文本、数值、手机、邮箱",
  "subtype": "text",
  "required": false
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- label:输入框标题
- name:输入框name属性
- value:输入框value属性
- placeholder:输入框为空时的提示信息
- subtype:输入框input的type属性
- required:是否必填（必填时标题前会有红色小星星）

**2.下拉框**

```
{
  "active": true,
  "type": "select",
  "label": "下拉框",
  "name": "select",
  "value": "",
  "placeholder": "请选择",
  "required": false,
  "data": [
    {
      "key": "option1",
      "value": "选项一",
      "selectd": false
    },
    {
      "key": "option2",
      "value": "选项二",
      "selectd": false
    }
  ]
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- label:下拉框标题
- name:下拉框name属性
- value:下拉框value属性（默认选中值）
- placeholder:下拉框为空时的选择提示信息
- required:是否必填（必填时标题前会有红色小星星）
- data:下拉项配置

**3.单选框**

```
{
  "active": true,
  "type": "radio",
  "label": "单选框",
  "name": "radio",
  "value": "",
  "required": false,
  "data": [
    {
      "key": "radio1",
      "value": "按钮一",
      "checked": false,
      "disabled": false
    },
    {
      "key": "radio2",
      "value": "按钮二",
      "checked": false,
      "disabled": false
    }
  ]
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- label:单选框标题
- name:单选框name属性
- value:单选框的value属性（默认选中值）
- required:是否必填（必填时标题前会有红色小星星）
- data:单选项配置

**4.复选框**

```
{
  "active": true,
  "type": "checkbox",
  "label": "复选框",
  "name": "checkbox",
  "values": [],
  "required": false,
  "tag": false,
  "data": [
    {
      "key": "checkbox1",
      "value": "复选一",
      "checked": false,
      "disabled": false
    },
    {
      "key": "checkbox2",
      "value": "复选二",
      "checked": false,
      "disabled": false
    }
  ]
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- label:复选框标题
- name:复选框name属性
- values:复选框的选中值数组（默认选中值）
- required:是否必填（必填时标题前会有红色小星星）
- tag:是否开启标签样式（开启后复选框较大更易操作）
- data:复选项配置

**5.文本框**

```
{
  "active": true,
  "type": "textarea",
  "label": "文本框",
  "name": "textarea",
  "value": "",
  "placeholder": "输入文本",
  "rows": "4",
  "required": false
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- label:文本框标题
- name:文本框name属性
- value:文本框内容
- placeholder:文本框为空时提示信息
- rows:文本框显示行数
- required:是否必填（必填时标题前会有红色小星星）

**6.引用框**


```
{
  "active": true,
  "type": "blockquote",
  "text": "引用框",
  "style-nm": false
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- text:引用框文本
- style-nm:是否开启边框模式

**7.字符集**

```
{
  "active": true,
  "type": "fieldset",
  "title": "字符集",
  "text": "提示内容"
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- title:字符集标题
- text:字符集提示内容

**8.分割线**

```
{
  "active": true,
  "type": "hr",
  "color": "",
  "color-tips": [
    "layui-border-red",
    "layui-border-orange",
    "layui-border-green",
    "layui-border-cyan",
    "layui-border-blue",
    "layui-border-black"
  ]
}
```
- active:是否活跃组件（一般可忽略）
- type:组件类型（一般可忽略）
- color:分割线颜色class（缺省灰色）
- color-tips:分割线可用颜色class